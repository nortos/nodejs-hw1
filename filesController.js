const fs = require('fs');
const fsPromises = fs.promises;

const regExp = /\S+\.(log|txt|json|yaml|xml|js)$/i;

const getAllFiles = async (req, res) => {
    try {
        const files = await fsPromises.readdir(`${__dirname}/files`);

        res.status(200).json({ message: 'Success', files: files.filter(item => item.match(regExp)) });
    } catch (err) {
        res.status(500).json({ message: 'Server error' });
    }
}

const createFile = async (req, res) => {
    const { filename, content } = req.body;

    try {
        await fsPromises.writeFile(`./files/${filename}`, content, 'utf-8');
        res.status(200).json({ message: 'File created successfully' });
    } catch (err) {
        res.status(500).json({ message: 'Server error' });
    }
}

const getFile = async (req, res) => {
    const currentFile = req.params.filename;

    try {
        const content = await fsPromises.readFile(`./files/${currentFile}`, 'utf8');
        const uploadedDate = (await fsPromises.stat(`./files/${currentFile}`)).birthtime;

        res.status(200).json({
            message: 'Success',
            filename: currentFile,
            content: content,
            extension: currentFile.substring(currentFile.length, currentFile.lastIndexOf('.') + 1),
            uploadedDate: uploadedDate
        });
    } catch (err) {
        res.status(500).json({ message: 'Server error' });
    }
}

const modifyFile = async (req, res) => {
    const currentFile = req.params.filename;
    const content = req.body.content;

    try {
        await fsPromises.writeFile(`./files/${currentFile}`, content, 'utf-8');
        res.status(200).json({ message: `File's content has been changed` });
    } catch (err) {
        res.status(500).json({ message: 'Server error' });
    }
}

const deleteFile = async (req, res) => {
    const currentFile = req.params.filename;
    const currentPass = req.query.password;

    try {
        if (currentPass) {
            const passwordsDB = JSON.parse(await fsPromises.readFile(`./passwords.json`, 'utf8'));
            const filteredDB = passwordsDB.filter(item => item.filename !== currentFile);

            await fsPromises.writeFile(`./passwords.json`, `${JSON.stringify(filteredDB)}`, 'utf-8');
        }

        await fsPromises.unlink(`./files/${currentFile}`);
        res.status(200).json({ message: 'File deleted successfully' });
    } catch (err) {
        res.status(500).json({ message: 'Server error' });
    }
}

module.exports = {
    getAllFiles,
    createFile,
    getFile,
    modifyFile,
    deleteFile
}
