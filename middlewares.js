const fs = require('fs');
const fsPromises = fs.promises;

const regExp = /\S+\.(log|txt|json|yaml|xml|js)$/i;

const validateCreateFile = async (req, res, next) => {
    const { filename, content } = req.body;
    const files = await fsPromises.readdir(`${__dirname}/files`);

    if (!filename) {
        return res.status(400).json({ message: 'Incorrect "filename" field specified' });
    }
    if (!content) {
        return res.status(400).json({ message: 'Incorrect "content" field specified' });
    }
    if (!filename.match(regExp)) {
        return res.status(400).json({ message: 'Incorrect "filename" field specified' });
    }
    if (files.find(item => item === filename)) {
        return res.status(400).json({ message: `${filename} filename already exists!` });
    }
    next();
}

const validateGetFile = async (req, res, next) => {
    const currentFile = req.params.filename;
    const files = await fsPromises.readdir(`${__dirname}/files`);

    if (!currentFile.match(regExp)) {
        return res.status(400).json({ message: 'Incorrect "filename" field specified' });
    }
    if (!files.includes(currentFile)) {
        return res.status(400).json({ message: `No file with '${currentFile}' filename found` });
    }
    next();
}

const validateModifyFile = async (req, res, next) => {
    const content = req.body.content;
    const currentFile = req.params.filename;
    const files = await fsPromises.readdir(`${__dirname}/files`);

    if (!content) {
        return res.status(400).json({ message: 'Incorrect "content" field specified' });
    }
    if (!currentFile.match(regExp)) {
        return res.status(400).json({ message: 'Incorrect "filename" field specified' });
    }
    if (!files.find(item => item === currentFile)) {
        return res.status(400).json({ message: `${currentFile} doesn't exists!` });
    }
    next();
}

const validateDeleteFile = async (req, res, next) => {
    const currentFile = req.params.filename;
    const files = await fsPromises.readdir(`${__dirname}/files`);

    if (!currentFile.match(regExp)) {
        return res.status(400).json({ message: 'Incorrect "filename" field specified' });
    }
    if (!files.find(item => item === currentFile)) {
        return res.status(400).json({ message: `${currentFile} doesn't exists!` });
    }
    next();
}

const setPassword = async (req, res, next) => {
    const { filename, password } = req.body;

    if (!password) {
        return next();
    }
    if (!password.trim()) {
        return res.status(400).json({ message: 'Password can not be empty' })
    }
    try {
        const passwordsDB = JSON.parse(await fsPromises.readFile(`./passwords.json`, 'utf8'));

        if (passwordsDB) {
            passwordsDB.push({filename, password});
            await fsPromises.writeFile(`./passwords.json`, `${JSON.stringify(passwordsDB)}`, 'utf-8');
        }
    } catch (err) {
        return res.status(500).json({ message: 'Server error' })
    }
    next();
}

const checkPermission = async (req, res, next) => {
    const filename = req.params.filename;
    const password = req.query.password;

    try {
        const passwordsDB = JSON.parse(await fsPromises.readFile(`./passwords.json`, 'utf8'));
        const currentFile = passwordsDB.find(item => item.filename === filename);

        if (!currentFile) {
            return next();
        }
        if (currentFile.password !== password) {
            return res.status(401).json({ message: 'You have not permissions to manage this file' })
        }
    } catch (err) {
        return res.status(500).json({ message: 'Server error' })
    }
    next();
}

module.exports = {
    validateCreateFile,
    validateGetFile,
    validateModifyFile,
    validateDeleteFile,
    setPassword,
    checkPermission
}