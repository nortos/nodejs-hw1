const express = require('express');
const router = express.Router();

const { validateGetFile, validateCreateFile, validateModifyFile, validateDeleteFile, setPassword, checkPermission } = require('./middlewares');
const { getAllFiles, getFile, createFile, modifyFile, deleteFile } = require('./filesController');

router.get('/', getAllFiles);
router.post('/', validateCreateFile, setPassword, createFile);
router.get('/:filename', validateGetFile, checkPermission, getFile);
router.put('/:filename', validateModifyFile, checkPermission, modifyFile);
router.delete('/:filename', validateDeleteFile, checkPermission, deleteFile);

module.exports = router;
