const express = require('express');
const morgan = require('morgan');
const filesRouter = require('./filesRouter');
const fs = require('fs');
const app = express();

const port = 8080;

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', filesRouter);
app.use((req, res) => res.status(404).json({ message: 'Client error' }));

app.listen(port, () => {
  console.log(`The server is listened on the port ${port}`);
  fs.writeFileSync('./passwords.json', '[]');
});
